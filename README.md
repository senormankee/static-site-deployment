# Static Docs


Content is served from an S3 bucket via Cloudfront

## How to deploy

Simply add your content to the Public folder in this repo, commit your branch and execute the Deploy pipeline in gitlab-ci

The document root is set to the root of the public folder.

You can create folders and serve this content from cloudfront, simply create your folder in the public folder
i.e. public/my-folder would be accessible at https://docs.test.com/my-folder

To deploy to Staging/Live you must create a Tag of the Main branch, once created, the Staging/Live pipeline will be automatically created, you then need to run the deploy steps in that pipeline

## Domains

Content is served from the following environment specific domains

dev - https://docs.dev.test.com
test1 - https://docs.test1.test.com
staging - https://docs.staging.test.com
live - https://docs.test.com

