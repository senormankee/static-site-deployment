data "terraform_remote_state" "cluster" {
  backend = "s3"

  config = {
    bucket = "${var.environment}-${var.cluster_bucket_suffix}"
    key    = var.cluster_key
    region = var.region
  }
}

data "aws_vpc" "name" {
  tags = {
    "Name" = "Application Consumer VPC"
  }
}

data "aws_subnets" "public" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.name.id]
  }

  tags = {
    "aws:cloudformation:logical-id" = "PublicSubnet*"
  }
}
