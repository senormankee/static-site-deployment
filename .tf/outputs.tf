output "route53_record_name" {
  description = "The name of the record"
  value       = module.records.route53_record_name
}

output "route53_record_fqdn" {
  description = "FQDN built using the zone domain and name"
  value       = module.records.route53_record_fqdn
}

output "sub_domain" {
  description = "Project sub domain"
  value       = var.sub_domain
}

output "public_subnets" {
  description = "The name of the record"
  value       = data.aws_subnets.public.ids
}

output "acm_arn" {
  description = "FHIR Service ARNs"
  value       = aws_acm_certificate.docs.arn
}
