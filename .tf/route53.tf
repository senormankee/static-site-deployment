module "records" {
  source    = "./modules/terraform-aws-route53/modules/records"
  zone_name = data.terraform_remote_state.cluster.outputs.public_hosted_zone_name
  records = [
    {
      name = "docs"
      type = "A"
      alias = {
        name    = aws_cloudfront_distribution.docs_s3_distribution.domain_name
        zone_id = aws_cloudfront_distribution.docs_s3_distribution.hosted_zone_id
      }
    }

  ]
}
