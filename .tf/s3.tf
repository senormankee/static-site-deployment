resource "aws_s3_bucket" "docs_bucket" {
  bucket = local.s3_bucket_name
}

resource "aws_s3_bucket_acl" "docs_bucket_acl" {
  bucket = aws_s3_bucket.docs_bucket.bucket
  acl    = "private"
  depends_on = [aws_s3_bucket_ownership_controls.s3_bucket_acl_ownership]
}

# Resource to avoid error "AccessControlListNotSupported: The bucket does not allow ACLs"
resource "aws_s3_bucket_ownership_controls" "s3_bucket_acl_ownership" {
  bucket = aws_s3_bucket.docs_bucket.id
  rule {
    object_ownership = "ObjectWriter"
  }
}

resource "aws_s3_bucket_public_access_block" "docs_access_block" {
  bucket = aws_s3_bucket.docs_bucket.id

  block_public_acls   = true
  block_public_policy = true

  restrict_public_buckets = true
  ignore_public_acls      = true
}

data "aws_iam_policy_document" "s3_policy" {
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.docs_bucket.arn}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.docs_origin_access_identity.iam_arn]
    }
  }
}

resource "aws_s3_bucket_policy" "docs_bucket_s3_policy" {
  bucket = aws_s3_bucket.docs_bucket.id
  policy = data.aws_iam_policy_document.s3_policy.json
}
