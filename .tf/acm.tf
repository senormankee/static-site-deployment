resource "aws_acm_certificate" "test_docs" {
  provider          = aws.us-east-1
  domain_name       = "docs.${var.environment == "live" ? "" : "${var.environment}."}${var.domain_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "validation" {
  provider = aws
  for_each = {
    for dvo in aws_acm_certificate.test_docs.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = data.terraform_remote_state.cluster.outputs.test_public_hosted_zone_id
}

resource "aws_acm_certificate_validation" "test_docs_certificate_validation" {
  provider                = aws.us-east-1
  certificate_arn         = aws_acm_certificate.test_docs.arn
  validation_record_fqdns = [for record in aws_route53_record.validation : record.fqdn]
}
