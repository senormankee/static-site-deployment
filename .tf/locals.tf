locals {
  s3_origin_id   = "test-docs"
  domain_name    = format("%s.%s", var.sub_domain, var.domain_name)
  s3_bucket_name = "${var.sub_domain}-${var.environment == "live" ? "test-com" : "${var.environment}-test-com"}"
}
