variable "sub_domain" {
  default     = "docs"
  description = "Subdomain for the FQDN"
}

variable "table_billing_mode" {
  description = "Controls how you are charged for read and write throughput and how you manage capacity."
  default     = "PROVISIONED"
}

variable "cluster_bucket_suffix" {
  default     = "terraform-bucket"
  type        = string
  description = "Bucket where terraform states reside"
}


variable "environment" {
  description = "Name of environment"
}

variable "region" {
  default     = "eu-west-2"
  description = "AWS region"
}

variable "domain_name" {
  default     = "test.com"
  description = "Domain Name of"
}
